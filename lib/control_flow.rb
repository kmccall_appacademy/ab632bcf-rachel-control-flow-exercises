# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  lowercase = ("A".."Z").to_a
  res = []
  arr = str.chars
  arr.map! do |el|
    if lowercase.include?(el)
      res << el
    end
  end
    res.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  arr = str.chars
  x = arr.length
  l = arr.length / 2
  if x.odd?
    return arr[l]
  else
    return arr [l - 1] + arr[l]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  vowels = ["a", "e", "i", "o", "u", "A", "E", "I", "O", "U"]
  arr = str.chars
  counter = 0
  arr.each do |el|
    if vowels.include?(el)
      counter += 1
    end
  end
  counter
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  r = (1..num).to_a
  r.reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  str = ""
  arr.each do |el|
    str << el + separator
  end
  if str[str.length - 1] == separator
    str.chop!
  end
  str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  arr = str.chars
  res = []
  arr.each_with_index do |el, i|
    if i.odd?
      res << el.upcase
    else
      res << el.downcase
    end
  end
  res.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  res = []
  arr = str.split(" ")
  arr.each do |el|
    if el.length >= 5
      res << el.reverse
    else
      res << el
    end
  end
  res.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  arr = (1..n).to_a
  res = []
  arr.each do |el|
    if el % 3 == 0  && el % 5 == 0
      res << "fizzbuzz"
    elsif el % 3 == 0
      res << "fizz"
    elsif el % 5 == 0
      res << "buzz"
    else
      res << el
    end
  end
  res
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  arr = (2...num).to_a
  res = []
  arr.each do |el|
    if num % el == 0
      res << el
    end
  end
  if num == 1
    return false
  elsif res.empty?
    return true
  else
    return false
  end
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  arr = (1..num).to_a
  res = []
  arr.each do |el|
    if num % el == 0
      res << el
    end
  end
  res
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  require 'prime'
  primes = Prime.each(num).to_a

  arr = []
  primes.each do |el|
    if num % el == 0
      arr << el
    end
  end
  arr
end


# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  require 'prime'
  primes = Prime.each(num).to_a

  arr = []
  primes.each do |el|
    if num % el == 0
      arr << el
    end
  end
  arr.count 
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  evens = []
  odds = []
  arr.each do |el|
    if el.odd?
      odds << el
    else
      evens << el
    end
  end
  if evens.length > odds.length
    return odds[0]
  else
    return evens[0]
  end
end
